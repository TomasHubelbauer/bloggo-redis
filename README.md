# Redis

## PubSub

- [Redis CLI reference](https://redis.io/topics/rediscli)

```sh
redis-cli help command
```

- A: `redis-cli subscribe news`
- B: `redis-cli publish news hello`

## Streams

- [ ] Take a look at Redis Streams

[Coming in Redis 5.0](http://antirez.com/news/116)

[Hacker News thread](https://news.ycombinator.com/item?id=16232946)

## WSL

### How to configure Redis-Server on WSL by *Jessica Deen*

- [Technet](https://blogs.technet.microsoft.com/jessicadeen/uncategorized/how-to-configure-redis-server-on-bash-on-ubuntu-on-windows-10-wsl/)
- [Author's Site](http://jessicadeen.com/tech/how-to-configure-redis-server-on-bash-on-ubuntu-on-windows-10-wsl/)

```sh
apt install redis-server
# change `bind 127.0.0.1` to `bind 0.0.0.0`
nano /etc/redis/redis.conf
service redis-server restart
redis-cli
```

If you get *Authentication failure* on `service redis-server restart`, just `logout` and log back in or use `sudo`.

Test in Redis CLI:

```redis
set boo "test"
get boo
```

- [ ] See if [`SHUTDOWN`](https://redis.io/commands) could be used instead of `service redis-server restart`
